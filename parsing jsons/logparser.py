''' Слияние логов
Имеется два файла с логами в формате JSONL, пример лога:
…
{"timestamp": "2021-02-26 08:59:20", "log_level": "INFO", "message": "Hello"}
{"timestamp": "2021-02-26 09:01:14", "log_level": "INFO", "message": "Crazy"}
{"timestamp": "2021-02-26 09:03:36", "log_level": "INFO", "message": "World!"}
…

Сообщения в заданных файлах упорядочены по полю timestamp в порядке возрастания.

Требуется написать скрипт, который объединит эти два файла в один.
При этом сообщения в получившемся файле тоже должны быть упорядочены в порядке возрастания по полю timestamp.
Время работы не должно превышать пяти минут

К заданию прилагается вспомогательный скрипт на python3, который создает два файла "log_a.jsonl" и "log_b.jsonl".

Командлайн для запуска:
log_generator.py <path/to/dir>

Ваше приложение должно поддерживать следующий командлайн:
<your_script>.py <path/to/log1> <path/to/log2> -o <path/to/merged/log>
'''


import argparse
import json
from datetime import datetime

start_time = datetime.now()


def yilder(file):
    for line in file:
        yield json.loads(line)

def nexter(gene):
    try:
        _ = next(gene)
    except StopIteration:
        _ = {}
    return _

parser = argparse.ArgumentParser(description='json parser')

parser.add_argument('file1', help='First log file path')
parser.add_argument('file2', help='Second log file path')

parser.add_argument('-o', dest='merged_file_path', help='Merged file path')

args = parser.parse_args()


with open(args.file1) as f1,\
        open(args.file2) as f2,\
        open(args.merged_file_path, 'w') as f3:
    first_gener = yilder(f1)
    second_gener = yilder(f2)
    first_item = nexter(first_gener)
    second_item = nexter(second_gener)

    while first_item or second_item:
        if first_item and second_item:

            if first_item['timestamp'] <= second_item['timestamp']:
                f3.write(str(first_item)+'\n')
                first_item = nexter(first_gener)

            elif first_item['timestamp'] > second_item['timestamp']:
                f3.write(str(second_item)+'\n')
                second_item = nexter(second_gener)

        elif first_item:
            f3.write(str(first_item)+'\n')
            for item in first_gener:
                f3.write(str(item)+'\n')
            first_item = nexter(first_gener)

        elif second_item:
            f3.write(str(second_item)+'\n')
            for item in second_gener:
                f3.write(str(item)+'\n')
            second_item = nexter(second_gener)

        elif not first_item and not second_item:
            break


end_time = datetime.now()
print(end_time-start_time)


